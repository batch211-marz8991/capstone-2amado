const express = require("express");
const router = express.Router();

const productController = require("../controllers/productControllers");
const auth = require("../auth");

//router for creating product
router.post("/",auth.verify,(req,res)=>{

	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data);
	productController.addProduct(data).then(resultFromController=>res.send(resultFromController))
});

//retrieve all active product route
router.get("/active",(req,res)=>{
	productController.getAllActiveProduct().then(resultFromController=>res.send(resultFromController))
});

//retrieving all products routes
router.get("/all",(req,res)=>{
	productController.getAllProduct().then(resultFromController=>res.send(resultFromController))
});

//retrieve specific product route
router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController));
});


//updating product information route
router.put("/:productId",auth.verify,(req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

// Route for deleting a product
router.delete("/delete/:id",auth.verify,(req,res)=>{
	const admin = auth.decode(req.headers.authorization).isAdmin;
	productController.deleteProduct(req.params.id).then(resultFromController=>res.send(resultFromController));
	
})


//archiving product route
router.put("/archive/:id",(req,res)=>{
	productController.archiveProduct(req.params.id).then(resultFromController=>res.send(resultFromController));
})


//activate product route
router.put("/activate/:id",(req,res)=>{
	productController.activateProduct(req.params.id).then(resultFromController=>res.send(resultFromController));
})
module.exports = router;